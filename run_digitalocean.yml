---

- name: Deploy kernel tests at DigitalOcean
  hosts: local
  tasks:

    - name: Ensure ssh key is added
      digital_ocean:
        state: present
        command: ssh
        name: gitlab_ssh_key
        ssh_pub_key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxJxVpdE41PzinWqz7t5bdqyEvxeRIKBat3vYrZEiD80u3a7ttER0pPUzpoD7ekj1XGO3rkyzKKTOzNQjV0AGiP/aOM1t+vu7BRAQoCiUmEwstTgl/L5qMdsKoGS8LRxesqGvJz/u+w6D9MtqEGHXY/j80M+wxUQfeX6qGLJJdiFw+VCVtNDLn7rFz0Kl7KAkIWaq5PsplBqE4/FZSHTPrJ4hYXUyYu5fMpNOYp6S9pRLxgVvJQxeJ4gM5/9iMb1G0pVirUL2GcZoiooYn8NvPofiIfttE3MrqK9Jxv3hSYiWFW7ugSHhbswVTnmF7SkL9vdAaZKS7TV74wDT3vOzD major@indium"
        api_token: "{{ ansible_env.DO_API_KEY }}"
      register: gitlab_ssh_key

    - name: Create droplet at DigitalOcean
      digital_ocean:
        state: present
        command: droplet
        name: "kerneltest-{{ ansible_env.CI_PIPELINE_ID }}"
        api_token: "{{ ansible_env.DO_API_KEY }}"
        size_id: c-8
        region_id: nyc3
        # image_id: centos-7-x64
        image_id: 43661891
        # image_id: Fedora-Cloud-Base-29-1.2.x86_64.qcow2
        unique_name: yes
        ssh_key_ids: "{{ gitlab_ssh_key.ssh_key.id }}"
        wait_timeout: 500
      register: my_droplet

    - name: Add droplet to group
      add_host:
        hostname: "{{ my_droplet.droplet.ip_address }}"
        groupname: digitalocean_droplets

    - name: Wait for ssh
      wait_for:
        host: "{{ my_droplet.droplet.ip_address }}"
        port: 22
        delay: 30
        timeout: 320
        state: started

- name: Prep host for Ansible
  hosts: digitalocean_droplets
  user: root
  gather_facts: false
  tasks:

    - name: Ensure Ansible packages are installed
      raw: "dnf -y groupinstall 'Ansible node'"

- name: Update host
  hosts: digitalocean_droplets
  user: root
  tasks:
    - name: Ensure dnf is configured to use fastestmirror
      ini_file:
        path: /etc/dnf/dnf.conf
        section: main
        option: fastestmirror
        value: 1
      when: ansible_pkg_mgr == 'dnf'

    - name: Update packages
      package:
        name: "*"
        state: latest
      register: package_updates

    - name: Ensure EPEL is enabled
      package:
        name: epel-release
        state: present
      when: ansible_distribution == 'CentOS'

    - name: Create swap file
      command: dd if=/dev/zero of=/swapfile bs=1M count=2048 status=progress
      args:
        creates: /swapfile
      register: created_swap

    - name: Set permissions on swap file
      file:
        path: /swapfile
        mode: '0600'

    - name: Run mkswap
      command: mkswap /swapfile
      register: mkswap_run
      when: created_swap is changed

    - name: Activate swap
      command: swapon /swapfile
      when: mkswap_run is changed

    - name: Add swap to /etc/fstab
      mount:
        src: /swapfile
        name: swap
        fstype: swap
        state: present

    - name: Reboot if packages were updated
      reboot:
        reboot_timeout: 300
        test_command: "uname -a"
      when: package_updates is changed

- name: Run kernel tests
  hosts: digitalocean_droplets
  user: root
  roles:
    - ltp
    - container
  pre_tasks:

    - name: Create directory to hold logs from tests
      file:
        path: test_logs
        state: directory
